import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:wothoqtest/screen/store/view.dart';

import 'constants/MyColors.dart';


void main() {
  runApp(EasyLocalization(
    child: MyApp(),
    supportedLocales: [Locale('ar', 'EG'), Locale('en', 'US')],
    path: 'assets/langs',
    fallbackLocale: Locale('ar', 'EG'),
    startLocale: Locale('ar', 'EG'),
  ));
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  static final navigatorKey = new GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      navigatorKey: navigatorKey,
      title: "",
      theme: ThemeData(
        primarySwatch: Colors.grey,
        cursorColor: MyColors.primary,
        focusColor: MyColors.primary,
        accentColor: MyColors.primary,
        primaryColor: MyColors.primary,
        platform: TargetPlatform.android,
        pageTransitionsTheme: PageTransitionsTheme(builders: {
          TargetPlatform.android: CupertinoPageTransitionsBuilder(),
        }),
      ),
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      home: Store(),

    );
  }
}
