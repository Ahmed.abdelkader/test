import 'package:flutter/material.dart';
import 'package:wothoqtest/constants/MyColors.dart';
import 'package:wothoqtest/constants/MyText.dart';
import 'package:wothoqtest/screen/productDetails/model.dart';
import 'package:easy_localization/easy_localization.dart';

class ProductDetails extends StatefulWidget {
  final StoreModel storeModel;

  const ProductDetails({Key key, this.storeModel}) : super(key: key);
  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {


  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(widget.storeModel.productDetails.length, (index) => _buildItem(
          color: index.isEven?MyColors.white:MyColors.grey.withOpacity(.1),
          detail: widget.storeModel.productDetails[index].value,
          name:context.locale==Locale("en","US")?widget.storeModel.productDetails[index].nameEn: widget.storeModel.productDetails[index].nameAr
      )),
    );
  }

  Widget _buildItem({Color color,String name,String detail}){
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 10),
      color: color,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          MyText(
            title: name,
            color: MyColors.black,
            size: 12,
          ),
          MyText(
            title: detail,
            color: MyColors.black,
            size: 12,
          ),
        ],
      ),
    );
  }
}
