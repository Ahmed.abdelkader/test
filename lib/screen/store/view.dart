import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:wothoqtest/constants/MyColors.dart';
import 'package:wothoqtest/constants/MyText.dart';
import 'package:wothoqtest/screen/productDetails/controller.dart';
import 'package:wothoqtest/screen/productDetails/model.dart';
import 'package:wothoqtest/screen/productDetails/view.dart';
import 'package:wothoqtest/screen/productReview/view.dart';

class Store extends StatefulWidget {
  @override
  _StoreState createState() => _StoreState();
}

class _StoreState extends State<Store> with TickerProviderStateMixin {
  TabController _controller;
  final StoreController storeController = StoreController();
  StoreModel _storeModel = StoreModel();
  bool loading = true;
  _changLang(){
    if(context.locale==Locale("en","US")){
      context.locale=Locale('ar', 'EG');
    }else{
      context.locale=Locale('en', 'US');

    }
  }

  Future<void> getStore() async {
    StoreModel storeModel = await storeController.getStoreModel();
    setState(() {
      _storeModel = storeModel;
      loading = false;
    });
  }

  @override
  void initState() {
    getStore();
    _controller = TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.white,
      body: Container(
        child: loading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height-(kToolbarHeight),
                    child: Stack(
                      alignment: Alignment.topCenter,
                      children: [
                        _buildHeader(),
                        Positioned(
                          top: MediaQuery.of(context).size.height * .28,
                          child: Container(
                            height: MediaQuery.of(context).size.height * .72,
                            color: MyColors.gold.withOpacity(.1),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                _buildDetails(),
                                _buildDesc(),
                                _buildTabBar(),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
      ),
    );
  }

  Widget _buildHeader() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * .3,
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          Swiper(
            itemBuilder: (BuildContext context, int index) {
              return Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * .3,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(
                          _storeModel.images[index].img,
                        ),
                        fit: BoxFit.fill)),
              );
            },
            autoplay: false,
            itemCount: _storeModel.images.length,
            scrollDirection: Axis.horizontal,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 20,
                ),
                Container(
                    padding: const EdgeInsets.all(5),
                    margin: const EdgeInsets.symmetric(horizontal: 5),
                    decoration: BoxDecoration(
                        color: MyColors.white, shape: BoxShape.circle),
                    child: Icon(
                      Icons.favorite_border,
                      size: 20,
                    )),
                Container(
                    margin: const EdgeInsets.symmetric(horizontal: 5),
                    padding: const EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        color: MyColors.white, shape: BoxShape.circle),
                    child: Icon(
                      Icons.share,
                      size: 20,
                    )),InkWell(
                  onTap: ()=>_changLang(),
                  child: Container(
                      margin: const EdgeInsets.symmetric(horizontal: 5),
                      padding: const EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          color: MyColors.white, shape: BoxShape.circle),
                      child: Icon(
                        Icons.language,
                        size: 20,
                      )),
                ),
                Spacer(),
                Container(
                    width: 60,
                    padding: const EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      borderRadius:context.locale==Locale("en","US")?BorderRadius.only(
                          topLeft: Radius.circular(30),
                          bottomLeft: Radius.circular(30)): BorderRadius.only(
                          topRight: Radius.circular(30),
                          bottomRight: Radius.circular(30)),
                      color: MyColors.white,
                    ),
                    child: Icon(
                      Icons.arrow_forward,
                      size: 20,
                    )),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildDetails() {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20), color: MyColors.white),
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MyText(
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            color: MyColors.blackOpacity,
            size: 12,
            title: _storeModel.name,
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              MyText(
                title: "${_storeModel.salePrice} ${tr("SAR")}",
                color: MyColors.blackOpacity,
                size: 12,
              ),
              SizedBox(
                width: 10,
              ),
              MyText(
                title: "${_storeModel.regularPrice} ${tr("SAR")}",
                color: MyColors.grey,
                size: 8,
                decoration: TextDecoration.lineThrough,
              ),
              // SizedBox(
              //   width: 10,
              // ),
              // MyText(
              //   title: "${_storeModel.}%",
              //   color: MyColors.gold,
              //   size: 10,
              // ),
              Spacer(),
              Row(
                children: [
                  RatingBar.builder(
                    initialRating: _storeModel.rate.toDouble(),
                    minRating: 0,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemSize: 15,
                    itemCount: 5,
                    tapOnlyMode: true,
                    unratedColor: MyColors.greyWhite,
                    itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: null,
                  ),
                  MyText(
                    title: "( ${_storeModel.numUsersRate} )",
                    color: MyColors.grey,
                    size: 10,
                  ),
                ],
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Divider(
              color: MyColors.grey,
              thickness: 1.5,
            ),
          ),
          ListTile(
            contentPadding: const EdgeInsets.all(0),
            leading: CircleAvatar(
              backgroundImage: NetworkImage(_storeModel.store.logo),
              maxRadius: 30,
            ),
            title: Expanded(
              child: MyText(
                title: _storeModel.store.name,
                size: 14,
                color: MyColors.blackOpacity,
              ),
            ),
            subtitle: Row(
              children: [
                Icon(
                  Icons.location_on_outlined,
                  color: MyColors.gold,
                ),
                Expanded(
                  child: MyText(
                    title: _storeModel.store.fullAddress,
                    size: 10,
                    maxLines: 1,
                    color: MyColors.grey.withOpacity(.8),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildDesc() {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20), color: MyColors.white),
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
      child: MyText(
        title: _storeModel.desc,
        size: 11,
        color: MyColors.black,
      ),
    );
  }

  Widget _buildTabBar() {
    return Container(
        margin: const EdgeInsets.symmetric(vertical: 10),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20), color: MyColors.white),
        padding: const EdgeInsets.symmetric( vertical: 15),
        child: Column(
          children: [
            TabBar(
              labelColor: MyColors.gold,
              indicatorSize: TabBarIndicatorSize.tab,
              unselectedLabelColor: MyColors.black,
              indicatorColor: MyColors.gold,
              controller: _controller,
              onTap: (int index){
                setState(() {
                  _controller.index=index;
                });
              },
              physics: NeverScrollableScrollPhysics(),
              indicator: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(color: MyColors.primary, width: 1))),
              tabs: [
                Tab(
                    child: MyText(
                  title: tr("productDetails"),
                  size: 11,
                )),
                Tab(
                    child: MyText(
                      title: tr("productReview"),
                  size: 11,
                )),
              ],
            ),
            Visibility(visible: _controller.index == 0,
            child: ProductDetails(storeModel: _storeModel,),
              replacement: ProductReview(storeModel: _storeModel,),
            )
          ],
        ));
  }
}
