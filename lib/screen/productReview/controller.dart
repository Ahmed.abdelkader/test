import 'package:wothoqtest/constants/Http.dart';

import 'model.dart';

class StoreController{
  Future<StoreModel> getStoreModel() async {
    String url = "v3/1243be15-efd5-4132-9bd0-eeb33f325f51";
    var _data = await Http().get(url, {});
    if (_data != null) {
      return StoreModel.fromJson(_data);
    } else {
      return StoreModel();
    }
  }
}