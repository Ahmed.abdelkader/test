import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:wothoqtest/constants/MyColors.dart';
import 'package:wothoqtest/constants/MyText.dart';
import 'package:wothoqtest/screen/productDetails/model.dart';

class ProductReview extends StatefulWidget {
  final StoreModel storeModel;

  const ProductReview({Key key, this.storeModel}) : super(key: key);
  @override
  _ProductReviewState createState() => _ProductReviewState();
}

class _ProductReviewState extends State<ProductReview> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(widget.storeModel.productDetails.length, (index) {
        return Column(
          children: [
            _buildItem(
                comment: widget.storeModel.reviews[index].review,
                date: widget.storeModel.reviews[index].createdAt,
                rate: widget.storeModel.reviews[index].rate.toDouble(),
                name: widget.storeModel.reviews[index].userName
            ),
            index==widget.storeModel.reviews.length-1?Container(height: .1,):Divider(thickness: 1,)
          ],
        );
      } ),
    );
  }

  Widget _buildItem({String name,double rate,String comment,String date}){
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MyText(
                title: name,
                color: MyColors.blackOpacity,
                size: 12,
              ),
              RatingBar.builder(
                initialRating: rate,
                minRating: 0,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemSize: 15,
                itemCount: 5,
                tapOnlyMode: true,
                unratedColor: MyColors.greyWhite,
                itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
                onRatingUpdate: null,
              )
            ],
          ),
          MyText(
            title: comment,
            color: MyColors.black,
            size: 11,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              MyText(
                title: date,
                color: MyColors.grey,
                size: 10,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
