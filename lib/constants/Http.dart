import 'dart:convert';

import 'package:http/http.dart' as http;
class Http {
  String baseUrl = "https://run.mocky.io/";


  get(String url, Map<String, String> body) async {
    var response = await http.post("$baseUrl$url", body: body,);
    print(response.statusCode);
    if (response.statusCode == 200) {
      var data = json.decode(response.body);
      return data;

    }
  }
}