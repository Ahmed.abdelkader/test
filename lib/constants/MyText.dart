import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class MyText extends StatelessWidget {
  String title;
  Color color = Colors.black;
  double size = 16;
  TextAlign alien = TextAlign.start;
  TextDecoration decoration = TextDecoration.none;
  TextOverflow overflow;
  bool isNumber;
  FontWeight fontWeight;
  String fontFamily;
  int maxLines;

  MyText(
      {this.title,
      this.fontFamily,
      this.color,
      this.size,
      this.alien,
      this.decoration,
      this.overflow,
        this.maxLines,
      this.isNumber = false,
      this.fontWeight = FontWeight.bold});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Text(
      "$title",
      textAlign: alien,
      textScaleFactor: 1.2,
      maxLines: maxLines,
      style: TextStyle(
          color: color,
          fontSize: size,
          fontFamily: fontFamily ?? "regular",
          decoration: decoration,
          fontWeight: fontWeight),
      overflow: overflow,
    );
  }
}
